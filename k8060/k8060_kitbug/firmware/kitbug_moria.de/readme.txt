Kitbug was published by National Semiconductor in appendix B
of the "SC/MP Kit Users Manual", see the PDF version in the
orig directory.

Kitbug as distributed uses 110 baud 8N2 (actually more like 8N3) on a 1
MHz SC/MP (= 2 MHz SC/MP II).  Other speeds were selected by
patching the ROM image.

See the original source code and the original ROM as well as
speed patched binaries in the directory orig.

Kitbug had a bug in the GECO routine, which does not wait
for a bit time after setting the MSB before the stop bit
is set, which leads to a set MSB in all echoed characters.

The kitbug.asm in the top level directory has this bug
fixed and is patched for 1200 baud (SC/MP II 4 MHz).

The speed patches are:

400/0x190 402/0x192: GECO 1/2 bit time (AC DLY)
413/0x19d 415/0x19f: GECO 1 bit time (AC DLY)
444/0x1bc: GECO stop bit (DLY only)
455/0x1c7 457/0x1c9: PUTC stop bit (AC DLY)
467/0x1d3 469/0x1d5: PUTC 1 bit time (AC DLY)

The SC/MP II needs four clock cycles per micro cycle, whereas the
original SC/MP only needs two clock cycles per micro cycle:

SC/MP DLY:    2 * (13 + 2*AC + 2*DLY + 512*DLY) clock cycles
SC/MP II DLY: 4 * (13 + 2*AC + 2*DLY + 512*DLY) clock cycles

Time in s = clock cycles * 1/Clockrate [Hz]

The bit rate is always little bit larger to take loop overhead
into account, the more so the faster the speed is.

110 baud 8N2 @ 2 MHz SC/MP II:

PUTC stop bit: 0xff 0x17 = 24,7 ms
PUTC 1 bit time: 0x8a 0x08 = 8.8 ms = 113.6 bit/s

1200 baud 8N2 @ 4 MHz SC/MP II (tested):

PUTC stop bit: 0x25 0x03 = 1,63 ms
PUTC 1 bit time: 0x50 0x01 = 0.687 ms = 1455 bit/s

KITBUG.ROM 110 baud 8N1 @ 1 MHz SCMP
400/0x190: 0x57
402/0x192: 0x04
413/0x19d: 0x7e
415/0x19f: 0x08
444/0x1bc: 0x08
455/0x1c7: 0xff
457/0x1c9: 0x17
467/0x1d3: 0x8a
469/0x1d5: 0x08

kitbug4MHZ110baud.bin
400: 0xb5
402: 0x08
413: 0x45
415: 0x11
444: 0x11
455: 0xff
457: 0x2e
467: 0x54
469: 0x11

kitbug4MHZ300baud.bin
400: 0x29
402: 0x03
413: 0x11
415: 0x06
444: 0x06
455: 0x6c
457: 0x06
467: 0x21
469: 0x06

kitbug4MHZ1200baud.bin
400: 0xa7
402: 0x00
413: 0x45
415: 0x01
444: 0x04
455: 0x25
457: 0x03
467: 0x50
469: 0x01

kitbug4MHZ2400baud.bin
400: 0x3d
402: 0x00
413: 0x80
415: 0x00
444: 0x02
455: 0x82
457: 0x01
467: 0x81
469: 0x00
