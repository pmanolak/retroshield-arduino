:0500395A76C03F0286C13F02B401A40D5874040A3F02B405000D2526C33F02B4A70D5B75040A3F02B41B5A474F20415741592C49274D2042555359210D05:00000000


To get this working, please do the following:

We want to initiate Load Tape command and then paste the code into the terminal.
You need to increase the inter-character delay to at least 100ms so 2650 can keep up.

Let me know how it goes.
Stay safe,
Erturk

1. Set up your terminal program so inter-character & inter-line delays are at least 100ms.
2. Then at the * prompt, type L and hit enter.
3. Copy/paste the code into the terminal.  (text doesn't show up. you should see Arduino transmitting characters.)
4. It should return with a prompt.
5. Type G0500 [Enter]
6. You need to enter a name or some text. Then hit enter.
7. It will respond with "GO AWAY, I'm BUSY" :):)

*L

*G0500
ÿHELLO
GO AWAY,I'M BUSY!


Paste this into the terminal after L [enter]
:0500395A76C03F0286C13F02B401A40D5874040A3F02B405000D2526C33F02B4A70D5B75040A3F02B41B5A474F20415741592C49274D2042555359210D05:00000000
