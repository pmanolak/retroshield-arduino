Pass one output. The<> are to show non-printing characters.
 This would be anything less than 20H. In other words 0Dh 
and 0Ah would be <D><A>. The <>s are not in the real output.
I can do a dump without them is needed. My code adds the <>s
so I know what it is sending.The text 0D0A are added by me
anytime there is a 0A in the output.




/ This is a comment<D><A>
<1>   0:<2> = 0<D><A>
<1>   0:<2>/<D><A>
<1>   0:<2>L0  NOP<D><A>
<1>   1:<2>/<D><A>
<1>   1:<2>L1 JCN 0 L1<D><A>
<1>   3:<2>L2  FIM 0 L2<D><A>
<1>   5:<2> SRC 0 L2<D><A>
<1>   6:<2>L3  FIN 0 L3<D><A>
<1>   7:<2> JIN 0 L3<D><A>
<1>   8:<2> JIN 7 L3<D><A>
<1>   9:<2>L4  JUN L4<D><A>
<1>  11:<2>L5  JMS L5<D><A>
<1>  13:<2> INC 0<D><A>
<1>  14:<2> INC 1<D><A>
<1>  15:<2>L6  ISZ 0 L6<D><A>
<1>  17:<2> ADD 14<D><A>
<1>  18:<2> SUB 0<D><A>
<1>  19:<2> LD 0<D><A>
<1>  20:<2> XCH 0<D><A>
<1>  21:<2> BBL 0<D><A>
<1>  22:<2> LDM 0<D><A>
<1>  23:<2> WRM<D><A>
<1>  24:<2> WMP<D><A>
<1>  25:<2> WRR<D><A>
<1>  26:<2> WPM<D><A>
<1>  27:<2> WR0<D><A>
<1>  28:<2> WR1<D><A>
<1>  29:<2> WR2<D><A>
<1>  30:<2> WR3<D><A>
<1>  31:<2> SBM<D><A>
<1>  32:<2> RDM<D><A>
<1>  33:<2> RDR<D><A>
<1>  34:<2> ADM<D><A>
<1>  35:<2> RD0<D><A>
<1>  36:<2> RD1<D><A>
<1>  37:<2> RD2<D><A>
<1>  38:<2> RD3<D><A>
<1>  39:<2> CLB<D><A>
<1>  40:<2> CLC<D><A>
<1>  41:<2> IAC<D><A>
<1>  42:<2> CMC<D><A>
<1>  43:<2> CMA<D><A>
<1>  44:<2> RAL<D><A>
<1>  45:<2> RAR<D><A>
<1>  46:<2> TCC<D><A>
<1>  47:<2> DAC<D><A>
<1>  48:<2> TCS<D><A>
<1>  49:<2> STC<D><A>
<1>  50:<2> DAA<D><A>
<1>  51:<2> KBP<D><A>
<1>  52:<2> DCL<D><A>
<1>  53:<2>$<D><0><A>
<0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><
><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0>
0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0><0
<0><0><0><D><0><A>
