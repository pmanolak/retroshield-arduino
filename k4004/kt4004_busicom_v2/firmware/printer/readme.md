Thanks to Tim McNerney for making the printer emulator available.


Original files are from:
https://www.4004.com/busicom-schematics-and-simulator.html

//-------------------------------------------------------------------------------
// Shinshu Seiki Model-102 printer emulator and exerciser software for PIC18F2320
// Version 1.0.0, released on November 15, 2008.
// Written for CCS C compiler (http://www.ccsinfo.com/)
// Author: Tim McNerney
//-------------------------------------------------------------------------------
//
// LEGAL NOTICE, DO NOT REMOVE
// 
// Notice: This software and documentation is provided "AS IS."  There
//         is no warranty, claims of accuracy or fitness for any
//         purpose other than for education.  The authoritative version
//         of this file can be found at http://www.4004.com
//  
// You are free:
//  
// * to copy, distribute, display, and perform this work
// * to make derivative works
//  
// Under the following conditions:
//  
//   Attribution.  You must attribute the work in the manner specified
//                 by the author or licensor.
//  
//   Noncommercial. You may not use this work for commercial purposes.
//  
//   Share Alike.  If you alter, transform, or build upon this work, you
//                 may distribute the resulting work only under a
//                 license identical to this one.
//  
// * For any reuse or distribution, you must make clear to others the
//   license terms of this work.
// * Any of these conditions can be waived if you get permission from
//   the copyright holder.
//  
// Your fair use and other rights are in no way affected by the above.
// 
// This is a human-readable summary of the Legal Code (the full license)
// available at:
//
//   http://creativecommons.org/licenses/by-nc-sa/2.5/legalcode
//-------------------------------------------------------------------------------
// model-102-exerciser.c -- Exerciser for Shinshu-Seiki Model 102 printer sim
//-------------------------------------------------------------------------------